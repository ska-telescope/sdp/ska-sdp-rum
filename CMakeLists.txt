project(rum)

cmake_minimum_required(VERSION 3.17.5)

# Set cmake module path
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/modules")

# Set build type
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  set(CMAKE_BUILD_TYPE
      Release
      CACHE STRING "Default build type." FORCE)
endif()

# Set debug print info
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")

# Enable OpenMP
find_package(OpenMP REQUIRED)

# Enable PThreads
find_package(Threads REQUIRED)

# Add RUM common directory as include directory
include_directories(${CMAKE_SOURCE_DIR}/common)

option(BUILD_PYTHON_RUM "" OFF)

# ##############################################################################
# More options
# ##############################################################################

set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH True)

if(${BUILD_PYTHON_RUM})
  find_package(
    Python3
    COMPONENTS Interpreter Development
    REQUIRED)
  find_package(pybind11 CONFIG REQUIRED)
endif()

# RUM interface and common functionality
add_subdirectory(common)

# RUM library
add_library(rum SHARED $<TARGET_OBJECTS:rum-common>)

# CPU
add_subdirectory(cpu)

# Disk
add_subdirectory(disk)

# Dummy
add_subdirectory(dummy)

# Memory
add_subdirectory(memory)

# Network
add_subdirectory(network)

# Python Bindings
if(${pybind11_FOUND})
  add_subdirectory(python)
endif()

# Public header file
set_target_properties(rum PROPERTIES PUBLIC_HEADER rum.hpp)

# Install library
install(
  TARGETS rum
  LIBRARY DESTINATION lib
  PUBLIC_HEADER DESTINATION include)
