# Ska Sdp Rum
[![pipeline status](https://gitlab.com/ska-telescope/sdp/ska-sdp-rum/badges/main/pipeline.svg)](https://gitlab.com/ska-telescope/sdp/ska-sdp-rum/-/commits/main)
[![Latest Release](https://gitlab.com/ska-telescope/sdp/ska-sdp-rum/-/badges/release.svg)](https://gitlab.com/ska-telescope/sdp/ska-sdp-rum/-/releases)

RUM is a high-level software library capable of collecting CPU/Disk/Memory/Network usage. The library provides a standard interface to easily measure the above mentioned metrics.

# Installation
First clone the repository:
```
git clone https://gitlab.com/ska-telescope/sdp/ska-sdp-rum
```

To build the software, run the following commands:
1. Set-up cmake in a build directory and cd into the directory, e.g. `~/ska-sdp-rum/build`
     * `cmake <source dir path> -DCMAKE_INSTALL_PREFIX=<install dir path>`
2. Optionally further configure cmake through interactive build settings or with command line variables
     * use `ccmake` and/or add `-DBUILD_PYTHON=<0 or 1>` to the `cmake` commandline to build the python bindings..
3. make and install
     * `make install`

# Usage
Include the header file into your program, e.g.:
```
#include rum.hpp”
```
Depending on which RUM implementations you have selected during the build, you can now initialize
any RUM instance:
```
std::unique ptr<rum::RUM> sensor(rum::cpu::CPU::create());
```
Next, you can start measuring resource usage using the common api as specified in `rum.hpp`.
Refer to `rum-test.cpp` as reference on how to use the library to dump results to a file.

# rum-test
RUM-test executables might be used directly to read the . The RUM-test executables are available in the install directory `/bin`.
