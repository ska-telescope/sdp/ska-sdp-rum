FROM rockylinux:8

RUN yum update -y &&\
    yum install -y epel-release &&\
    yum update -y &&\
    yum --enablerepo epel groupinstall -y "Development Tools" &&\
    yum --enablerepo epel install -y cmake findutils gcc-c++ gcc gcc-gfortran git gnupg2 hostname iproute make patch python3 python3-pip python3-setuptools unzip wget