FROM ubuntu:22.04

RUN apt-get update &&\
    DEBIAN_FRONTEND=noninteractive apt-get install -y bash build-essential ca-certificates cmake coreutils curl environment-modules gfortran git gpg lsb-release python3 python3-distutils python3-pip python3-venv unzip zip wget&&\
    rm -rf /var/apt/cache