#include <chrono>
#include <iostream>
#include <sstream>
#include <thread>

#include "rum-test.hpp"

void run(rum::RUM &sensor, int argc, char *argv[]) {
  char *dumpFileName = std::getenv("RUM_DUMPFILE");
  sensor.startDumpThread(dumpFileName);

  std::cout << "Running RUM" << std::endl;

  if (argc == 1) {
    auto first = sensor.read();
    while (true)
      ;
  } else {
    std::stringstream command;
    for (int i = 1; i < argc; i++) {
      if (i > 1) {
        command << " ";
      }
      command << argv[i];
    }
    if (system(command.str().c_str()) != 0) {
      perror(command.str().c_str());
    }
  }
}
