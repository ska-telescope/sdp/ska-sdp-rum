#include "rum.hpp"

namespace rum {
RUM::~RUM() { stopDumpThread(); };

double RUM::seconds(const State &firstState, const State &secondState) {
  return secondState.timeAtRead - firstState.timeAtRead;
};

void RUM::startDumpThread(const char *dumpFileName) {
  if (!dumpFileName) {
    dumpFileName = getDumpFileName();
  }
  dumpFile = std::unique_ptr<std::ofstream>(new std::ofstream(dumpFileName));

  dumpThread = std::thread([&] {
    State startState = read(), currentState = startState;
    updateHeader();
    dump_header();
    int numberMetrics = getNumberMetrics();

    const char *dumpIntervalOverrideStr = std::getenv("DUMP_INTERVAL_OVERRIDE");
    int dumpIntervalOverride = -1;
    if (dumpIntervalOverrideStr != nullptr) {
      dumpIntervalOverride = std::stoi(dumpIntervalOverrideStr);
    }
    int dumpInterval = getDumpInterval();

    State aggregateState;
    aggregateState.metrics.resize(numberMetrics);
    std::fill(aggregateState.metrics.begin(), aggregateState.metrics.end(), 0);
    int counter = 0;
    previousState = startState;
    while (!stop) {
      std::this_thread::sleep_for(std::chrono::milliseconds(dumpInterval));
      currentState = read();
      if (dumpIntervalOverride > dumpInterval) {
        counter++;
        for (int i = 0; i < currentState.metrics.size(); i++) {
          aggregateState.metrics[i] += currentState.metrics[i];
        }
        if (counter >= dumpIntervalOverride / dumpInterval) {
          counter = 0;
          aggregateState.timeAtRead = currentState.timeAtRead;
          if (averageResults()) {
            for (int i = 0; i < numberMetrics; i++) {
              aggregateState.metrics[i] = aggregateState.metrics[i] /
                                          (dumpIntervalOverride / dumpInterval);
            }
          }

          if (previousState.timeAtRead != currentState.timeAtRead) {
            dump(startState, aggregateState);
          }

          std::fill(aggregateState.metrics.begin(),
                    aggregateState.metrics.end(), 0);
        }
      } else {
        if (previousState.timeAtRead != currentState.timeAtRead) {
          dump(startState, currentState);
        }
      }
      previousState = currentState;
    }
  });
}

void RUM::stopDumpThread() {
  stop = true;
  if (dumpThread.joinable()) {
    dumpThread.join();
  }
}

void RUM::dump_header() {
  if (dumpFile != nullptr) {
    std::unique_lock<std::mutex> lock(dumpFileMutex);
    *dumpFile << "S ";
    for (int i = 0; i < header.size(); i++) {
      if (i > 0) {
        *dumpFile << " ";
      }
      *dumpFile << header[i];
    }
    *dumpFile << std::endl;
  }
}

void RUM::dump(const State &startState, const State &secondState) {
  if (dumpFile != nullptr) {
    std::unique_lock<std::mutex> lock(dumpFileMutex);
    *dumpFile << std::fixed << std::setprecision(8)
              << seconds(startState, secondState) << " ";
    // std::cout << "secondState.metrics.size() " << secondState.metrics.size()
    // << std::endl;
    for (int i = 0; i < secondState.metrics.size(); i++) {
      *dumpFile << std::fixed << std::setprecision(2);
      if (i > 0) {
        *dumpFile << " ";
      }
      *dumpFile << secondState.metrics[i];
    }
    *dumpFile << std::endl;
  }
}

// void RUM::mark(const State &startState, const State &currentState,
//                const char *name, unsigned tag) const {
//   if (dumpFile != nullptr) {
//     std::unique_lock<std::mutex> lock(dumpFileMutex);
//     *dumpFile << "M " << currentState.timeAtRead - startState.timeAtRead << '
//     '
//               << tag << " \"" << (name == nullptr ? "" : name) << '"'
//               << std::endl;
//   }
// }

double RUM::get_wtime() {
  return std::chrono::duration_cast<std::chrono::microseconds>(
             std::chrono::system_clock::now().time_since_epoch())
             .count() /
         1.0e6;
}

State RUM::read() { return stop ? previousState : measure(); }

} // end namespace rum
