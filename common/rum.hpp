#pragma once

#include <iostream>

#include <fstream>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>
#include <string>

#include <iomanip>
#include <algorithm>

namespace rum {

class State {
 public:
  double timeAtRead;
  std::vector<double> metrics;
};

class RUM {
 public:
  virtual ~RUM();

  virtual State read();

  static double seconds(const State &firstState, const State &secondState);

  std::vector<std::string> header;

  void startDumpThread(const char *dumpFileName);
  void stopDumpThread();

 protected:
  virtual State measure() = 0;

  virtual const char *getDumpFileName() = 0;

  virtual int getDumpInterval() = 0;

  virtual bool averageResults() = 0;

  virtual int getNumberMetrics() = 0;

  virtual bool updateHeader() = 0;

  void aggregateMetrics(State currentState, int numberMetrics, int dumpIntervalOverride);

  void dump_header();

  void dump(const State &startState, const State &secondState);

  static double get_wtime();

 private:
  std::thread dumpThread;
  std::unique_ptr<std::ofstream> dumpFile = nullptr;
  mutable std::mutex dumpFileMutex;
  volatile bool stop = false;
  State previousState;
};
}  // end namespace rum
