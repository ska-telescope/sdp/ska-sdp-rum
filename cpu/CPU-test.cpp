#include "../common/rum-test.hpp"

#include "CPU.hpp"

int main(int argc, char **argv) {
  auto sensor = rum::cpu::CPU::create();
  run(*sensor, argc, argv);
}
