#include "CPU.hpp"
#include <cassert>

namespace rum {
namespace cpu {

int getAvailableCPUCount() {
  std::ifstream cpu_info("/proc/cpuinfo");
  int cpuCount = 0;
  std::string line;

  while (std::getline(cpu_info, line)) {
    if (line.substr(0, 9) == "processor") {
      cpuCount++;
    }
  }
  return cpuCount;
}

struct CPUStats {
  size_t user, nice, system, idle, iowait, irq, softirq, steal, guest,
      guest_nice;
};

class CPU_ : public CPU {
public:
  CPU_();
  ~CPU_();
  int nr_cpus;
  int nr_metrics;

private:
  class CPUState {
  public:
    operator State();
    double timeAtRead;
    std::vector<CPUStats> cpuStats;
    std::vector<double> metrics;
  };

  virtual State measure();

  virtual const char *getDumpFileName() { return "/tmp/CPU.out"; }

  virtual int getDumpInterval() { return 100; }

  virtual bool averageResults() { return true; }

  virtual int getNumberMetrics() { return nr_metrics; }

  CPUState previousState;
  CPUState read_cpu();
  size_t getTotalTime(const CPUStats &stats);
  size_t getActiveTime(const CPUStats &stats);
  std::vector<CPUStats> get_cpu_stats();
  std::string readProcStat();

  virtual bool updateHeader() {
    header.push_back("CPU_total");
    for (int i = 0; i < nr_cpus; ++i) {
      header.push_back("CPU_" + std::to_string(i));
    }
    return true;
  }
};

CPU_::CPUState::operator State() {
  State state;
  state.timeAtRead = timeAtRead;
  state.metrics = metrics;
  return state;
}

std::unique_ptr<CPU> CPU::create() { return std::unique_ptr<CPU>(new CPU_()); }

CPU_::CPU_() {
  nr_cpus = getAvailableCPUCount();
  nr_metrics = nr_cpus + 1;
  previousState.metrics.resize(nr_metrics);
  previousState.cpuStats.resize(nr_metrics);
  previousState = read_cpu();
}

CPU_::~CPU_() { stopDumpThread(); }

std::string CPU_::readProcStat() {
  std::ifstream procStatFile("/proc/stat");

  if (!procStatFile) {
    std::cerr << "Failed to open /proc/stat" << std::endl;
    return "";
  }

  std::stringstream buffer;
  buffer << procStatFile.rdbuf();

  return buffer.str();
}

std::vector<CPUStats> CPU_::get_cpu_stats() {
  std::string procStatContent = readProcStat();
  std::vector<CPUStats> stats;
  std::istringstream procStatStream(procStatContent);
  std::string line;

  while (std::getline(procStatStream, line)) {
    if (line.substr(0, 3) == "cpu") {
      std::istringstream lineStream(line);
      std::string cpuLabel;
      lineStream >> cpuLabel;

      if (cpuLabel == "cpu" || cpuLabel.find("cpu") == 0) {
        std::string time;
        std::vector<std::string> cpuTimes;

        while (lineStream >> time) {
          cpuTimes.push_back(time);
        }

        CPUStats cpu;

        cpu.user = std::stoul(cpuTimes[0]);
        cpu.nice = std::stoul(cpuTimes[1]);
        cpu.system = std::stoul(cpuTimes[2]);
        cpu.idle = std::stoul(cpuTimes[3]);
        cpu.iowait = std::stoul(cpuTimes[4]);
        cpu.irq = std::stoul(cpuTimes[5]);
        cpu.softirq = std::stoul(cpuTimes[6]);
        cpu.steal = std::stoul(cpuTimes[7]);
        cpu.guest = std::stoul(cpuTimes[8]);
        cpu.guest_nice = std::stoul(cpuTimes[9]);
        stats.push_back(cpu);
      }
    }
  }

  return stats;
}

CPU_::CPUState CPU_::read_cpu() {
  CPUState state;
  state.timeAtRead = get_wtime();
  state.cpuStats = get_cpu_stats();

  for (int i = 0; i < nr_cpus + 1; ++i) {
    size_t total_time_cur = getTotalTime(state.cpuStats[i]);
    size_t active_time_cur = getActiveTime(state.cpuStats[i]);

    size_t total_time_prev = getTotalTime(previousState.cpuStats[i]);
    size_t active_time_prev = getActiveTime(previousState.cpuStats[i]);

    assert(total_time_cur - total_time_prev == 0);

    double active_ratio = (double)(active_time_cur - active_time_prev) /
                          (double)(total_time_cur - total_time_prev);

    state.metrics.push_back(active_ratio);
  }

  previousState = state;

  return state;
}

size_t CPU_::getTotalTime(const CPUStats &stats) {
  return stats.user + stats.nice + stats.system + stats.idle + stats.iowait +
         stats.irq + stats.softirq + stats.steal;
}

size_t CPU_::getActiveTime(const CPUStats &stats) {
  return getTotalTime(stats) - stats.idle;
}

State CPU_::measure() { return read_cpu(); }

} // end namespace cpu
} // end namespace rum
