#pragma once

#include <numeric>
#include "rum.hpp"

namespace rum {
namespace cpu{
class CPU : public RUM {
public:
  static std::unique_ptr<CPU> create();
};
} // end namespace cpu
} // end namespace rum



