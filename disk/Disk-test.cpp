#include "../common/rum-test.hpp"

#include "Disk.hpp"

int main(int argc, char **argv) {
  auto sensor = rum::disk::Disk::create();
  run(*sensor, argc, argv);
}
