#include "Disk.hpp"
#include <algorithm>
#include <cassert>
#include <cstring>
#include <fstream>
#include <string>

namespace rum {
namespace disk {

std::vector<std::string> getDisks() {
  std::vector<std::string> diskNames;
  std::ifstream diskstats("/proc/diskstats");
  std::string line;
  while (std::getline(diskstats, line)) {
    std::istringstream iss(line);
    std::string diskName;
    iss >> diskName >> diskName >> diskName;
    if (diskName.find("sd") == 0 || diskName.find("nvme") == 0) {
      diskNames.push_back(diskName + "_RD");
      diskNames.push_back(diskName + "_WR");
      diskNames.push_back(diskName + "_I/O");
    }
  }
  return diskNames;
}

class Disk_ : public Disk {
public:
  Disk_();
  ~Disk_();
  int nr_metrics;
  std::vector<std::string> disks;

private:
  class DiskState {
  public:
    operator State();
    double timeAtRead;
    std::vector<double> diskStat;
    std::vector<double> metrics;
  };

  virtual State measure();

  virtual const char *getDumpFileName() { return "/tmp/Disk.out"; }

  virtual int getDumpInterval() { return 100; }

  virtual bool averageResults() { return false; }

  virtual int getNumberMetrics() { return disks.size(); }

  DiskState previousState;
  DiskState read_disk();

  virtual bool updateHeader() {
    for (auto &disk : disks) {
      header.push_back(disk);
    }

    return true;
  }
};

Disk_::DiskState::operator State() {
  State state;
  state.timeAtRead = timeAtRead;
  state.metrics = metrics;
  return state;
}

std::unique_ptr<Disk> Disk::create() {
  return std::unique_ptr<Disk>(new Disk_());
}

Disk_::Disk_() {
  disks = getDisks();
  nr_metrics = disks.size();
  previousState.metrics.resize(nr_metrics);
  previousState.diskStat.resize(nr_metrics);
  previousState = read_disk();
}

Disk_::~Disk_() { stopDumpThread(); }

std::vector<double> get_disk_times() {
  std::vector<double> disk_times;
  std::ifstream diskstats("/proc/diskstats");
  std::string line;

  while (std::getline(diskstats, line)) {
    std::istringstream iss(line);
    std::string diskName;
    double reads_completed, reads_merged, sectors_read, time_reading;
    double writes_completed, writes_merged, sectors_written, time_writing;
    double io_in_progress, time_io, time_weighted_io;
    iss >> diskName >> diskName >> diskName;
    if (diskName.find("sd") == 0 || diskName.find("nvme") == 0) {
      iss >> reads_completed >> reads_merged >> sectors_read >> time_reading >>
          writes_completed >> writes_merged >> sectors_written >>
          time_writing >> io_in_progress >> time_io >> time_weighted_io;

      disk_times.push_back(time_reading);
      disk_times.push_back(time_writing);
      disk_times.push_back(time_io);
    }
  }
  return disk_times;
}

Disk_::DiskState Disk_::read_disk() {

  DiskState state;
  state.timeAtRead = get_wtime();
  state.diskStat = get_disk_times();

  state.metrics.resize(nr_metrics);
  std::fill(state.metrics.begin(), state.metrics.end(), 0);

  for (int i = 0; i < nr_metrics / 3; i++) {
    double time_reading_cur = state.diskStat[i * 3];
    double time_writing_cur = state.diskStat[i * 3 + 1];
    double time_io_cur = state.diskStat[i * 3 + 2];

    double time_reading_prev = previousState.diskStat[i * 3];
    double time_writing_prev = previousState.diskStat[i * 3 + 1];
    double time_io_prev = previousState.diskStat[i * 3 + 2];

    double time_reading_delta = time_reading_cur - time_reading_prev;
    double time_writing_delta = time_writing_cur - time_writing_prev;
    double time_io_delta = time_io_cur - time_io_prev;

    state.metrics[i * 3] = time_reading_delta;
    state.metrics[i * 3 + 1] = time_writing_delta;
    state.metrics[i * 3 + 2] = time_io_delta;
  }

  previousState = state;

  return state;
}

State Disk_::measure() { return read_disk(); }

} // end namespace disk
} // end namespace rum
