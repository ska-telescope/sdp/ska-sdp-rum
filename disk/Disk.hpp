#pragma once

#include <numeric>
#include "rum.hpp"

namespace rum {
namespace disk{
class Disk : public RUM {
public:
  static std::unique_ptr<Disk> create();
};
} // end namespace disk
} // end namespace rum



