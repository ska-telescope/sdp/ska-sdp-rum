#include "../common/rum-test.hpp"

#include "Dummy.hpp"

int main(int argc, char **argv) {
  auto sensor = rum::dummy::Dummy::create();
  run(*sensor, argc, argv);
}
