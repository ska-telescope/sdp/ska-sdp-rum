#include "Dummy.hpp"

namespace rum {
namespace dummy {

class Dummy_ : public Dummy {
private:
  virtual State measure();

  virtual const char *getDumpFileName() { return nullptr; }

  virtual int getDumpInterval() { return 100; }

  virtual bool averageResults() { return false; }

  virtual int getNumberMetrics() { return 2; }

  virtual bool updateHeader() {
    header.push_back("Dummy 0");
    header.push_back("Dummy 1");
    return true;
  }
};

std::unique_ptr<Dummy> Dummy::create() {
  return std::unique_ptr<Dummy>(new Dummy_());
}

State Dummy_::measure() {
  State state;
  state.timeAtRead = RUM::get_wtime();
  state.metrics.resize(2);
  std::fill(state.metrics.begin(), state.metrics.end(), 0);
  return state;
}

} // end namespace dummy
} // end namespace rum
