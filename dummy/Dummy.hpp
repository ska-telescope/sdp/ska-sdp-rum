#pragma once

#include "rum.hpp"

namespace rum {
namespace dummy{
class Dummy : public RUM {
public:
  static std::unique_ptr<Dummy> create();
};
} // end namespace dummy
} // end namespace rum



