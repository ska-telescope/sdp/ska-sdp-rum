#include "../common/rum-test.hpp"

#include "Memory.hpp"

int main(int argc, char **argv) {
  auto sensor = rum::memory::Memory::create();
  run(*sensor, argc, argv);
}
