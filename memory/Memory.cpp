#include "Memory.hpp"
#include <cassert>

namespace rum {
namespace memory {

class Memory_ : public Memory {
public:
  Memory_();
  ~Memory_();

private:
  int totalMemory;
  int totalSwap;
  class MemoryState {
  public:
    operator State();
    double timeAtRead;
    std::vector<int> memoryStat;
    std::vector<double> metrics;
  };

  virtual State measure();

  virtual const char *getDumpFileName() { return "/tmp/Memory.out"; }

  virtual int getDumpInterval() { return 100; }

  virtual bool averageResults() { return true; }

  virtual int getNumberMetrics() { return 2; }

  MemoryState previousState;
  MemoryState read_memory();

  std::vector<int> get_memory();
  int get_val(const std::string &target, const std::string &content);

  virtual bool updateHeader() {
    header.push_back("Memory");
    header.push_back("Swap");
    return true;
  }
};

Memory_::MemoryState::operator State() {
  State state;
  state.timeAtRead = timeAtRead;
  state.metrics.reserve(2);
  state.metrics = metrics;
  return state;
}

std::unique_ptr<Memory> Memory::create() {
  return std::unique_ptr<Memory>(new Memory_());
}

Memory_::Memory_() {
  std::ifstream proc_meminfo("/proc/meminfo");
  std::string content((std::istreambuf_iterator<char>(proc_meminfo)),
                      std::istreambuf_iterator<char>());
  totalMemory = get_val("MemTotal:", content);
  totalSwap = get_val("SwapTotal:", content);
  proc_meminfo.close();

  previousState.memoryStat.resize(2);
  std::fill(previousState.memoryStat.begin(), previousState.memoryStat.end(),
            0);
  previousState = read_memory();
}

Memory_::~Memory_() { stopDumpThread(); }

std::vector<int> Memory_::get_memory() {
  std::vector<int> result;
  std::ifstream proc_meminfo("/proc/meminfo");

  std::string content((std::istreambuf_iterator<char>(proc_meminfo)),
                      std::istreambuf_iterator<char>());

  result.push_back(get_val("MemAvailable:", content));
  result.push_back(get_val("SwapFree:", content));

  return result;
}

int Memory_::get_val(const std::string &target, const std::string &content) {
  int result = -1;
  std::size_t start = content.find(target);
  if (start != std::string::npos) {
    int begin = start + target.length();
    std::size_t end = content.find("kB", start);
    std::string substr = content.substr(begin, end - begin);
    result = std::stoi(substr);
  }
  return result;
}

Memory_::MemoryState Memory_::read_memory() {
  MemoryState state;
  state.timeAtRead = get_wtime();
  state.memoryStat = get_memory();

  double averageMemoryAvailable =
      (state.memoryStat[0] + previousState.memoryStat[0]) / 2;
  double averageSwapAvailable =
      (state.memoryStat[1] + previousState.memoryStat[1]) / 2;

  double memoryUsage = (totalMemory - averageMemoryAvailable) / totalMemory;
  double swapUsage = (totalSwap - averageSwapAvailable) / totalSwap;

  state.metrics.push_back(memoryUsage);
  state.metrics.push_back(swapUsage);

  previousState = state;

  return state;
}

State Memory_::measure() { return read_memory(); }

} // end namespace memory
} // end namespace rum
