#pragma once

#include <numeric>
#include "rum.hpp"

namespace rum {
namespace memory{
class Memory : public RUM {
public:
  static std::unique_ptr<Memory> create();
};
} // end namespace memory
} // end namespace rum



