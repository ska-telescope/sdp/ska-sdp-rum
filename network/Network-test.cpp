#include "../common/rum-test.hpp"

#include "Network.hpp"

int main(int argc, char **argv) {
  auto sensor = rum::network::Network::create();
  run(*sensor, argc, argv);
}
