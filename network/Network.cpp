#include "Network.hpp"
#include <algorithm>
#include <cassert>
#include <cstring>
#include <fstream>
#include <string>

namespace rum {
namespace network {

std::vector<std::string> getInterfaces() {
  std::vector<std::string> interfaces;

  // Open the /proc/net/dev file for reading
  std::ifstream infile("/proc/net/dev");
  std::string line;

  // Skip the first two lines of the file
  getline(infile, line);
  getline(infile, line);

  // Loop through the remaining lines of the file and extract the interface
  // names
  while (getline(infile, line)) {
    std::string interface;
    if (line.find(":") != std::string::npos) {
      interface = line.substr(0, line.find(":"));
      interface.erase(remove_if(interface.begin(), interface.end(), isspace),
                      interface.end());
      interfaces.push_back(interface + "_RX");
      interfaces.push_back(interface + "_TX");
    }
  }

  return interfaces;
}

class Network_ : public Network {
public:
  Network_();
  ~Network_();
  int nr_metrics;
  std::vector<std::string> interfaces;

private:
  class NetworkState {
  public:
    operator State();
    double timeAtRead;
    std::vector<long> networkStat;
    std::vector<double> metrics;
  };

  virtual State measure();

  virtual const char *getDumpFileName() { return "/tmp/Network.out"; }

  virtual int getDumpInterval() { return 100; }

  virtual bool averageResults() { return false; }

  virtual int getNumberMetrics() { return interfaces.size(); }

  NetworkState previousState;
  NetworkState read_network();

  virtual bool updateHeader() {
    for (auto &interface : interfaces) {
      header.push_back(interface);
    }

    return true;
  }
};

Network_::NetworkState::operator State() {
  State state;
  state.timeAtRead = timeAtRead;
  state.metrics = metrics;
  return state;
}

std::unique_ptr<Network> Network::create() {
  return std::unique_ptr<Network>(new Network_());
}

Network_::Network_() {
  interfaces = getInterfaces();
  nr_metrics = interfaces.size();
  previousState.metrics.resize(nr_metrics);
  previousState.networkStat.resize(nr_metrics);
  previousState = read_network();
}

Network_::~Network_() { stopDumpThread(); }

std::vector<long> get_network_times() {
  std::vector<long> net_traffic;
  std::ifstream infile("/proc/net/dev");
  std::string line;

  // skip first two lines
  getline(infile, line);
  getline(infile, line);

  while (getline(infile, line)) {
    char interface[20]; // using char array instead of string
    long rx_bytes, tx_bytes;

    // extract interface name and traffic data
    sscanf(line.c_str(), "%s %ld %*d %*d %*d %*d %*d %*d %*d %ld", interface,
           &rx_bytes, &tx_bytes);
    net_traffic.push_back(rx_bytes);
    net_traffic.push_back(tx_bytes);
  }
  return net_traffic;
}

Network_::NetworkState Network_::read_network() {

  NetworkState state;
  state.timeAtRead = get_wtime();
  state.networkStat = get_network_times();

  state.metrics.resize(nr_metrics);
  std::fill(state.metrics.begin(), state.metrics.end(), 0);

  for (int i = 0; i < nr_metrics / 2; i++) {
    long rx_bytes_cur = state.networkStat[i * 2];
    long tx_bytes_cur = state.networkStat[i * 2 + 1];

    long rx_bytes_prev = previousState.networkStat[i * 2];
    long tx_bytes_prev = previousState.networkStat[i * 2 + 1];

    double rx_delta = rx_bytes_cur - rx_bytes_prev;
    double tx_delta = tx_bytes_cur - tx_bytes_prev;

    state.metrics[i * 2] = rx_delta;
    state.metrics[i * 2 + 1] = tx_delta;
  }

  previousState = state;

  return state;
}

State Network_::measure() { return read_network(); }

} // end namespace network
} // end namespace rum
