#pragma once

#include <numeric>
#include "rum.hpp"

namespace rum {
namespace network{
class Network : public RUM {
public:
  static std::unique_ptr<Network> create();
};
} // end namespace network
} // end namespace rum



