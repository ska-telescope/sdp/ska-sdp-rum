import time

import rum


@rum.dump("cpu", "cpu-stat.txt")
def my_kernel1():
    time.sleep(5)


if __name__ == "__main__":
    my_kernel1()
