#include <pybind11/pybind11.h>

#include <../common/rum.hpp>
#include <../cpu/CPU.hpp>
#include <../disk/Disk.hpp>
#include <../dummy/Dummy.hpp>
#include <../memory/Memory.hpp>
#include <../network/Network.hpp>

namespace py = pybind11;

double seconds(rum::State start, rum::State end) {
  return end.timeAtRead - start.timeAtRead;
}

PYBIND11_MODULE(pyrum, m) {
  m.doc() = "librum python bindings";

  m.def("seconds", &seconds, "Get elapsed time");

  py::class_<rum::State>(m, "State");

  py::class_<rum::cpu::CPU>(m, "CPU")
      .def("create", &rum::cpu::CPU::create)
      .def("read", &rum::cpu::CPU::read)
      .def("startDumpThread", &rum::cpu::CPU::startDumpThread)
      .def("stopDumpThread", &rum::cpu::CPU::stopDumpThread);

  py::class_<rum::disk::Disk>(m, "Disk")
      .def("create", &rum::disk::Disk::create)
      .def("read", &rum::disk::Disk::read)
      .def("startDumpThread", &rum::disk::Disk::startDumpThread)
      .def("stopDumpThread", &rum::disk::Disk::stopDumpThread);

  py::class_<rum::dummy::Dummy>(m, "Dummy")
      .def("create", &rum::dummy::Dummy::create)
      .def("read", &rum::dummy::Dummy::read)
      .def("startDumpThread", &rum::dummy::Dummy::startDumpThread)
      .def("stopDumpThread", &rum::dummy::Dummy::stopDumpThread);

  py::class_<rum::memory::Memory>(m, "Memory")
      .def("create", &rum::memory::Memory::create)
      .def("read", &rum::memory::Memory::read)
      .def("startDumpThread", &rum::memory::Memory::startDumpThread)
      .def("stopDumpThread", &rum::memory::Memory::stopDumpThread);

  py::class_<rum::network::Network>(m, "Network")
      .def("create", &rum::network::Network::create)
      .def("read", &rum::network::Network::read)
      .def("startDumpThread", &rum::network::Network::startDumpThread)
      .def("stopDumpThread", &rum::network::Network::stopDumpThread);
}
