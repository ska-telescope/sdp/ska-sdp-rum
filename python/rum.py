import pyrum
from pyrum import seconds


def get_rum(platform, device_id=0):
    if platform == "cpu":
        return pyrum.CPU.create()
    elif platform == "disk":
        return pyrum.Disk.create()
    elif platform == "memory":
        return pyrum.Memory.create()
    elif platform == "network":
        return pyrum.Network.create()


def dump(platform, filename, device_id=0):
    def decorator(func):
        def wrapper(*args, **kwargs):
            rum = get_rum(platform, device_id)

            if not filename:
                raise Exception("Please provide a filename to dump the results")
            rum.startDumpThread(filename)
            res = func(*args, **kwargs)
            rum.stopDumpThread()

            return res

        return wrapper

    return decorator
